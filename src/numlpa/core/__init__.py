# -*- coding: utf-8 -*-

"""Basic features of NumLPA.

This package gathers independent tools designed as re-usable software
components to be used in higher level functions.

"""
