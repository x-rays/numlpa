# -*- coding: utf-8 -*-

"""CPU analyzer.

"""

from . import analysis, parser


setup = parser.setup
analyze = analysis.analyze
