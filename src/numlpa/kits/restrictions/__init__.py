# -*- coding: utf-8 -*-

"""Fourier variable range restrictions.

This package gathers the different ways to limit the fit interval of
the LPA models on the diffraction profile.

"""
