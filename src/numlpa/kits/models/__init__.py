# -*- coding: utf-8 -*-

"""LPA theoretical models.

This package gathers the implementations of available LPA models for
the prediction of microstructure parameters from the Fourier transform
of the diffracted intensity.

"""
