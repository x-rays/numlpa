# -*- coding: utf-8 -*-

"""Dislocation probability distributions.

This package gathers the implementations of the probability
distributions used for the random drawing of dislocation samples.

"""
