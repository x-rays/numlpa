# -*- coding: utf-8 -*-

"""Multivariate function optimizers.

This package gathers the implementation of optimizers in charge of
finding the parameters minimizing a given objective function.

"""
