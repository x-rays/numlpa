Data formats
============

NumLPA produces data of different categories (mainly dislocation positions, simulation results and fit results).
During development, it was considered of primary importance that the user be able to replace, edit, load and reuse this data.
This led to the data formats presented here.

By default, files are exported in JSON format.
You will therefore be able to read and process the data produced by NumLPA with almost any other programming language.
The sections below are here to inform you of what data you can fetch from the files.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   dislocation-sample
   fourier-transform
   model-adjustment
   energy-evaluation
   spatial-analysis
