Regular installation
====================

This page shows how to install the package (in non-editable mode).
With this installation option, you won't be able to modify the package sources.

.. note::

   We strongly recommend using a virtual environment when installing a python package such as this one.
   **It is not necessary**, **but** if you don't know about virtual environments you may lose the opportunity to keep the Python packages you install on your computer tidy.
   To install this package in a virtual environment, follow the instructions on the :doc:`venv` page.

NumLPA is released on the `Python Package Index <https://pypi.org/project/numlpa>`_.
Thus, the easiest way to install the package is to use the `package installer for Python <https://pip.pypa.io/en/stable/>`_ with the following command:

.. code-block:: bash

   pip install numlpa

That's all!
The installation is complete and you can start using the package with the examples provided in the :doc:`../examples/index` section.
