Installation guide
==================

You will find here all the information and advice for the installation of the package.
Don't worry, it shouldn't take long!

.. note::

   You can install the package in two different ways: with or without the ability to modify the package source files.

   * If you don't need to modify the source files, follow the instructions in the :doc:`regular` page and skip the :doc:`editable` page.

   * If you want to install the package in editable mode, you can skip the :doc:`regular` page and go directly to the :doc:`editable` page.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   regular
   editable
