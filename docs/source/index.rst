NumLPA package documentation
============================

Welcome!
This documentation has been carefully prepared to help you use the NumLPA package.
The first section (:doc:`practice/index`) provides installation instructions and usage examples.
The second section (:doc:`package/numlpa`) documents the package code.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   practice/index
   package/numlpa

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
